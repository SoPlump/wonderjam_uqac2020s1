# WonderJAM_UQAC2020S1

## Présentation
### Quand la magie tourne mal !

Deux magiciens ayant l'habitude de s'entraider pour concocter leurs potions se retrouvent soudainement l'un contre l'autre après que leur boule de cristal se soit détraquée !
Attrapez votre manette afin de collecter tous les ingrédients nécessaires à votre potion et ainsi prouver que vous êtes le meilleur magicien ! Mais faites attention à ce que votre feu ne s'éteigne pas ou il pourrait vous arriver malheur !

Les sorts des magiciens sont nombreux ! Augmenter sa vitesse, téléporter son chaudron proche de soi ou alors au centre du jeu notamment. Mais il peut aussi décider d'embêter son nouvel adversaire en lui bloquant sa vision, en le ralentissant, en inversant les chaudrons pour le destabiliser, en téléportant le chaudron de son nouvel adversaire proche de soi ou alors en lui inversant complètement ses contrôles ! Ou alors un bon coup d'épaule avec une bonne vitesse à l'ancienne est aussi permis !



Que le meilleur magicien gagne !



Astuce :

Afin de maintenir le feu sous le chaudron,  du bois peut être coupé en interagissant face à un arbre et être déposé au chaudron.

Pistes d'amélioration :

- mieux expliquer le système de gestion du feu,

- rendre aléatoire la carte et l'emplacement des items,

- corriger bugs de déplacement et de collision avec l'environnement,

- ajouter de la juiciness au malus/bonus (icônes, son etc.).

Contraintes de styles de la Jam :  Exploration, Multijoueur, (Gestion)

Gameplay :

Jouable à deux manettes, une manette et un clavier, et à deux sur un clavier

Présenté par l'équipe Baguette !


## Authors

- Sylvain FRANCO
- Guillaume BLANC DE LANAUTE
- Mayeul MARSAUT
- Antonin GABORIAU
- Sophie RAUDRANT
- Pierre LEPERCQ
