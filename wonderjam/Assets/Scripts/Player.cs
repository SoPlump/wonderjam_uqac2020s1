﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject gameManager;

    public KeyCode touche_clavier_action;
    public KeyCode bouton_action = KeyCode.JoystickButton2;

    public KeyCode touche_clavier_drop;
    public KeyCode bouton_drop = KeyCode.JoystickButton3;

    public KeyCode touche_clavier_nav_inv1;
    public KeyCode bouton_nav_inv1 = KeyCode.JoystickButton14;

    public KeyCode touche_clavier_nav_inv2;
    public KeyCode bouton_nav_inv2;

    private new Collider collider;

    private PlayerController controller;

    private bool isCuttingWood;

    private Coroutine cutWoodRoutine;

    [SerializeField]
    private Cauldron myCauldron;

    private Inventory inventory;

    private int selectedItem = 0;

    private AudioSource perso_audioSource;

    [SerializeField]
    private AudioClip audioGrab = null;

    [SerializeField]
    private AudioClip audioChopWood = null;

    [SerializeField]
    private AudioClip audioAddWood = null;

    [SerializeField]
    private AudioClip audioGoodPutInFire = null;

    [SerializeField]
    private AudioClip audioWrongPutInFire = null;

    [SerializeField]
    private int respawnTime = 0;

    private Collider[] collidersSave = new Collider[4];

    // Start is called before the first frame update
    void Start()
    {
        inventory = GetComponent<Inventory>();

        controller = GetComponent<PlayerController>();

        perso_audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();

        HandleLayers();
    }

    private bool CheckRightTag(Collider other)
    {
        if (other.CompareTag("Wood") || other.CompareTag("Cauldron") || other.CompareTag("Ingredient"))
        {
            return true;
        }

        return false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (CheckRightTag(other) && (collider == null || Vector3.Distance(other.transform.position, transform.position) < Vector3.Distance(collider.transform.position, transform.position)))
        {
            collider = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (CheckRightTag(other) && collider != null)
        {
            collider = null;
        }
    }

    private void GetInput()
    {
        if (controller.ActionKey() && !controller.IsMoving && !isCuttingWood && collider != null)
        {
            StartAction();
        }

        if (controller.DropKey() && !isCuttingWood)
        {
            DropItems();
        }

        if(controller.SelectItem()) {
            if(++selectedItem > 3) {
                selectedItem = 0;
            }
        }
    }

    private void HandleLayers()
    {
        if (controller.IsMoving)
        {
            StopCutWood();
        }
    }

    private int CheckForEmptySlot()
    {
        for (int i = 0; i < 4; i++)
        {
            if (inventory.checkForInvItem(i) == null)
            {
                return i;
            }
        }

        return -1;
    }

    private void DropItems()
    {
        Item item = inventory.checkForInvItem(selectedItem);

        if (selectedItem >= 0 && item != null && item.food)
        {
            Vector3 position = transform.position;

            position.x += 50.0f;

            GameObject temp = collidersSave[selectedItem].gameObject;

            temp.GetComponent<Ingredient>().Move(position);

            while (temp.GetComponent<Ingredient>().currentCollisions != 0)
            {
                position.x += 5.0f;

                temp.GetComponent<Ingredient>().Move(position);
            }

            temp.SetActive(true);

            collidersSave[selectedItem] = null;

            inventory.RemoveInvItem(selectedItem);
        }
    }

    private void StartAction()
    {
        if (collider.CompareTag("Wood") && !controller.IsMoving)
        {
            if (CheckForEmptySlot() != -1)
            {
                cutWoodRoutine = StartCoroutine(CutWood());
            }
        }
        else if (collider.CompareTag("Cauldron") && myCauldron.CheckRecipesSize())
        {
            if (gameManager.GetComponent<Gamemode>().isMutual)
            {
                Cauldron mutualCauldron = gameManager.GetComponent<Gamemode>().mutualCauldron.GetComponent<Cauldron>();
                Item item = inventory.checkForInvItem(selectedItem);

                if (selectedItem >= 0 && item != null)
                {
                    if (!item.food)
                    {
                        perso_audioSource.PlayOneShot(audioWrongPutInFire);

                        inventory.RemoveInvItem(selectedItem);
                    }
                    else if (item.food)
                    {
                        if (mutualCauldron.CheckIngredient(item))
                        {
                            inventory.RemoveInvItem(selectedItem);
                            mutualCauldron.SendToCauldron();

                            perso_audioSource.PlayOneShot(audioGoodPutInFire);
                        }
                        else
                        {
                            perso_audioSource.PlayOneShot(audioWrongPutInFire);
                        }

                        inventory.RemoveInvItem(selectedItem);

                        StartCoroutine(RespawnObject(selectedItem));
                    }
                }
            }
            else if(collider.gameObject.transform.name == myCauldron.transform.name)
            {

                Item item = inventory.checkForInvItem(selectedItem);

                if (selectedItem >= 0 && item != null)
                {
                    if (!item.food)
                    {
                        perso_audioSource.PlayOneShot(audioAddWood);

                        myCauldron.fire.addWood();

                        inventory.RemoveInvItem(selectedItem);
                    }
                    else if (item.food)
                    {
                        if (myCauldron.CheckIngredient(item))
                        {
                            inventory.RemoveInvItem(selectedItem);
                            myCauldron.SendToCauldron();

                            perso_audioSource.PlayOneShot(audioGoodPutInFire);
                        }
                        else
                        {
                            perso_audioSource.PlayOneShot(audioWrongPutInFire);
                        }

                        inventory.RemoveInvItem(selectedItem);

                        StartCoroutine(RespawnObject(selectedItem));
                    }
                }
            }
        }
        else if (collider.CompareTag("Ingredient"))
        {
            int slot = CheckForEmptySlot();

            if (slot != -1)
            {
                inventory.GiveItem(slot, collider.name);

                perso_audioSource.PlayOneShot(audioGrab);

                HideObject(slot);
            }
        }
    }

    public void DropObjectOnGround()
    {
        Item item = inventory.checkForInvItem(selectedItem);

        if (selectedItem >= 0 && item != null)
        {
            if (item.food)
            {
                DropItems();
            }
            else
            {
                inventory.RemoveInvItem(selectedItem);
            }
        }
    }

    private IEnumerator CutWood()
    {
        isCuttingWood = true;

        perso_audioSource.PlayOneShot(audioChopWood);

        yield return new WaitForSeconds(2);

        inventory.GiveItem(CheckForEmptySlot(), collider.name);

        StartCoroutine(DespawnObject());

        StopCutWood();
    }

    private void HideObject(int slot)
    {
        if (slot != -1)
        {
            collidersSave[slot] = collider;
        }

        collider.gameObject.SetActive(false);

        collider = null;
    }

    private IEnumerator DespawnObject()
    {
        Collider save = collider;

        HideObject(-1);

        yield return new WaitForSeconds(respawnTime);

        save.gameObject.SetActive(true);
    }

    private IEnumerator RespawnObject(int id)
    {
        yield return new WaitForSeconds(respawnTime);

        collidersSave[id].gameObject.SetActive(true);

        collidersSave[id] = null;
    }

    private void StopCutWood()
    {
        if (isCuttingWood)
        {
            perso_audioSource.Stop();

            isCuttingWood = false;

            if (cutWoodRoutine != null)
            {
                StopCoroutine(cutWoodRoutine);
            }
        }
    }

    public int GetSelectedItem() {
        return selectedItem;
    }
}
