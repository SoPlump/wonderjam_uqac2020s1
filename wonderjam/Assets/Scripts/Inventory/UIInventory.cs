﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventory : MonoBehaviour {
    public List<UIItem> uIItems = new List<UIItem>();
    public GameObject slotPrefab;
    public GameObject borderPrefab;
    public Transform slotPanel;
    public Transform borderPanel;
    public int numberOfSlot = 6;

    private List<GameObject> borderInstances;



    private GameObject obj;
    private Player player;

    private int position = 0;

    [SerializeField]
    private int playerIndex;

    private void Awake() {
        if (playerIndex == 0)
            obj = GameObject.Find("Player1");
        else
            obj = GameObject.Find("Player2");

        player = obj.GetComponent<Player>();

        borderInstances = new List<GameObject>();

        for (int i = 0; i < numberOfSlot; i++) {
            GameObject borderInstance = Instantiate(borderPrefab,
                new Vector3(borderPanel.position.x, borderPanel.position.y, borderPanel.position.z),
                borderPanel.rotation);
            borderInstance.transform.SetParent(borderPanel);

            borderInstance.GetComponent<Image>().enabled = false;
            borderInstances.Add(borderInstance);

            GameObject instance = Instantiate(slotPrefab,
        new Vector3(slotPanel.position.x, slotPanel.position.y, slotPanel.position.z),
        slotPanel.rotation);
            instance.transform.SetParent(slotPanel);

            uIItems.Add(instance.GetComponentInChildren<UIItem>());
        }
    }

    private void Update() {
        ActivateBorder();
    }

    private void ActivateBorder() {
        for (int i = 0; i < numberOfSlot; i++) {
            if (i == player.GetSelectedItem())
                borderInstances[i].GetComponent<Image>().enabled = true;
            else
                borderInstances[i].GetComponent<Image>().enabled = false;
        }

    }

    public void UpdateSlot(int slot, Item item) {
        uIItems[slot].UpdateItem(item);
    }

    public void AddNewItem(Item item) {
        UpdateSlot(uIItems.FindIndex(i => i.item == null), item);
    }

    public void RemoveItem(int id) {
        UpdateSlot(id, null);
    }
}

