﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour {
    public List<Item> items = new List<Item>();
    public List<Item> items_forest = new List<Item>();
    public List<Item> items_lac = new List<Item>();
    public List<Item> items_flower = new List<Item>();
    public List<Item> items_rocks = new List<Item>();

    private void Awake() {
        BuildDatabse();
    }

    public Item GetItem(int id) {
        return items.Find(item => item.id == id);
    }

    public Item GetItem(string itemName) {
        return items.Find(item => itemName.Contains(item.title));
    }

    void BuildDatabse() {
        items = new List<Item>()
        {
            new Item(0,     "Poudre de farine",             "Quoi ? De la poudre de poudre ?",              new Vector3(1, 0, -1), true),
            new Item(1,     "Extrait de sezame",            "Plus rare que l'or.",                          new Vector3(1, 0, 1), true),
            new Item(2,     "Patte de lapin en chocolat",   "Il va etre vraiment en retard.",               new Vector3(1, 0, 1), true),
            new Item(3,     "Sauce balsamique enchantee",   "Le vinaigre n'apprecie pas.",                  new Vector3(1, 0, 1), true),
            new Item(4,     "Fleur delicate",               "Ne marchez pas dessus !",                      new Vector3(1, 0, 1), true),
            new Item(5,     "Fausse fleur",                 "Belle nature...",                              new Vector3(1, 0, 1), true),
            new Item(6,     "Grue en papier mouillee",      "L'eau est impregne d'une puissante magie.",    new Vector3(1, 0, 1), true),
            new Item(7,     "Casquette casse-brique",       "Son porteur a disparu dans la plomberie.",     new Vector3(1, 0, 1), true),
            new Item(8,     "Bonsai geant",                 "Dilemme du verre a moitie vide...",            new Vector3(1, 0, 1), true),
            new Item(9,     "Touffe de buisson commun",     "Rien de special, vraiment.",                   new Vector3(1, 0, 1), true),
            new Item(10,    "Bois vivant",                  "Appele aussi Saule-Cogneur.",                  new Vector3(1, 0, 1), false),
            new Item(11,    "Malachite",                    "Un classique !",                               new Vector3(1, 0, 1), true),
            new Item(12,    "Oeufs de grenouille",          "Hmmm, ça fretille !",                          new Vector3(1, 0, 1), true),
            new Item(13,    "Foin de bovin aile",           "Une race tres exigeante.",                     new Vector3(1, 0, 1), true),
            new Item(14,    "Essence d'essence",            "Peut la sentir a 10km a la ronde.",            new Vector3(1, 0, 1), true),
            new Item(15,    "Masque brise",                 "Son porteur devait etre minuscule.",           new Vector3(1, 0, 1), true),
            new Item(16,    "Recettes de grand-mere",       "Que faire sans la base des bases ?",           new Vector3(1, 0, 1), true),
            new Item(17,    "Gui feuillu",                  "Les druides en rafollent.",                    new Vector3(1, 0, 1), true),
            new Item(18,    "Minerai brillant",             "Si ça brille, c'est precieux !",               new Vector3(1, 0, 1), true),
            new Item(19,    "Fil d'araignee herbivore",     "Arachnophobes, vous allez adorer.",            new Vector3(1, 0, 1), true),
            new Item(20,    "Diamant",                      "Carbone a haute pression.",                    new Vector3(1, 0, 1), true),
        };

        items_forest = new List<Item> { items[8], items[9], items[17], items[19], items[2], items[15] };
        items_lac = new List<Item> { items[3], items[6], items[12], items[14] };
        items_flower = new List<Item> { items[1], items[4], items[5], items[13], items[16] };
        items_rocks = new List<Item> { items[0], items[7], items[11], items[18], items[20] };
    }
}
