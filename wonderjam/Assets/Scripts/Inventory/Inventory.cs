﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> characterItems = new List<Item>();
    public List<Item> groundItems = new List<Item>();
    public ItemDatabase itemDatabase;
    public UIInventory inventoryUI;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void GiveItem(int id)
    {
        Item itemToAdd = itemDatabase.GetItem(id);
        characterItems[id] = itemToAdd;
        Debug.Log("Siwe item: " + characterItems.Count);
        inventoryUI.AddNewItem(itemToAdd);
        Debug.Log("Added item: " + itemToAdd.title);
    }

    public void GiveItem(int id, string itemName)
    {
        print("yop 2 : " + id + " " + itemName);
        Item itemToAdd = itemDatabase.GetItem(itemName);
        print("yop 3 : " + itemToAdd.title);
        if (itemToAdd != null)
        {
            if (characterItems.Count > id)
            {
                characterItems[id] = itemToAdd;
            }
            else
            {
                characterItems.Add(itemToAdd);
            }
            inventoryUI.AddNewItem(itemToAdd);
            Debug.Log("Added item: " + itemToAdd.title);
        }
        else
        {
            Debug.Log("Item inexistant");
        }
    }
    
    public Item checkForInvItem(int id)
    {
        if (characterItems.Count > id)
        {
            return characterItems[id];
        }

        return null;
    }
    
    public void RemoveInvItem(int id)
    {
        Item itemToRemove = checkForInvItem(id);
        if(itemToRemove != null)
        {
            characterItems[id] = null;
            inventoryUI.RemoveItem(id);
            Debug.Log("Item removed : " + itemToRemove.title);
        }
    }

    public void InitItem(int id)
    {
        Item itemToAdd = itemDatabase.GetItem(id);
        groundItems.Add(itemToAdd);
        Debug.Log("Added item: " + itemToAdd.title);
    }

    public void InitItem(string itemName)
    {
        Item itemToAdd = itemDatabase.GetItem(itemName);
        groundItems.Add(itemToAdd);
        Debug.Log("Added item: " + itemToAdd.title);
    }

    public void RenderGroundItem(List<Item> groundItems)
    {

    }
    public Item CheckForGroundItem(int id)
    {
        return groundItems.Find(item => item.id == id);
    }
    public void RemoveGroundItem(int id)
    {
        Item itemToRemove = CheckForGroundItem(id);
        if(itemToRemove != null)
        {
            groundItems.Remove(itemToRemove);
            Debug.Log("Item removed : " + itemToRemove.title);
        }
    }

    public bool CheckDistance()
    {


        return true;
    }

}
