﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public int id;
    public string title;
    public string description;
    public Vector3 position;
    public Sprite icon;
    public bool food;

    public Item(int id, string title, string description, Vector3 position, bool food)
    {
        this.id = id;
        this.title = title;
        this.description = description;
        this.position = position;
        this.food = food;
        this.icon = Resources.Load<Sprite>("Sprites/Items/" + title);
    }
}