﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause_fcts : MonoBehaviour
{

    public void Menu()
    {
        SceneManager.LoadScene("Scenes/1_Intro");
    }

    public void Quit()
    {
        SceneManager.LoadScene("Scenes/3_Outro");
    }
}
