﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Bonus
{
    NONE,
    SPEED,
    BRING_CAULDRON,
    TELEPORT_CAULDRON_CENTER,
}

enum Malus
{
    NONE,
    INK,
    SPEED,
    INVERT,
    BRING_OTHER_CAULDRON,
    INVERT_CAULDRON
}

public class spells_manager : MonoBehaviour
{
    private GameObject gameManager;

    private Bonus bonus = Bonus.NONE;
    private Malus malus = Malus.NONE;

    public float timeToNextSpells = 8.0f;

    public GameObject otherPlayer;
    public PlayerController controller;

    public GameObject myCauldron;
    public GameObject otherCauldron;

    public GameObject dashParticles;


    /*
     * BONUS
     */

    // Add speed
    [Header("Bonus_SpeedUp")]
    public float speedToAdd = 50.0f;
    public float timeMoreSpeed = 10.0f;

    // Dash
    [Header("Bonus_Dash")]
    public float maxDashTime = 0.4f;
    public float dashSpeed = 100.0f;
    float dashDistance = 3.0f;
    float dashStoppingSpeed = 0.1f;


    /*
     * MALUS
     */

    // Ink
    [Header("Malus_Ink")]
    public GameObject tache_autre_joueur;
    public float timeInk = 3.0f;

    // Add speed
    [Header("Malus_SpeedDown")]
    public float speedToRemove = 30.0f;
    public float timeLessSpeed = 10.0f;

    [Header("Malus_InvertMovement")]
    public float timeInvert = 10f;

    public void Start()
    {
        controller = GetComponent<PlayerController>();
        gameManager = GameObject.Find("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.BonusKey())
        {
            if (bonus != Bonus.NONE)
            {
                switch(bonus)
                {
                    case Bonus.SPEED:
                        StartCoroutine(bonus_speed());
                        break;
                    case Bonus.BRING_CAULDRON:
                        StartCoroutine(bonus_bringCauldron());
                        break;
                    case Bonus.TELEPORT_CAULDRON_CENTER:
                        StartCoroutine(bonus_teleportCauldronCenter());
                        break;
                }
                bonus = Bonus.NONE;
                malus = Malus.NONE;
            }
            else
            {
                Debug.Log("No bonus for player " + transform.name);
            }
        }
        if (controller.MalusKey())
        {
            if (malus != Malus.NONE)
            {
                switch (malus)
                {
                    case Malus.INK:
                        StartCoroutine(malus_tache());
                        break;
                    case Malus.SPEED:
                        StartCoroutine(malus_speed());
                        break;
                    case Malus.INVERT:
                        StartCoroutine(malus_invert_movement());
                        break;
                    case Malus.BRING_OTHER_CAULDRON:
                        StartCoroutine(malus_bringOtherCauldron());
                        break;
                    case Malus.INVERT_CAULDRON:
                        malus_invert_cauldron();
                        break;
                }
                malus = Malus.NONE;
                bonus = Bonus.NONE;
            }
            else
            {
                Debug.Log("No malus for player " + transform.name);
            }
        }
        if (controller.DashKey())
        {
            StartCoroutine(dash_particles());
            StartCoroutine(bonus_dash());
        }
    }
    
    IEnumerator bonus_dash()
    {
        dashParticles.SetActive(true);
        controller.dashSpeed = controller.speed + dashSpeed;

        yield return new WaitForSeconds(maxDashTime);

        controller.dashSpeed = 0.0f;
        controller.dashDistance = 1.0f;
    }

    IEnumerator dash_particles()
    {
        controller.dashDistance = dashDistance;
        yield return new WaitForSeconds(maxDashTime + 0.1f);
        dashParticles.SetActive(false);
    }

    IEnumerator bonus_bringCauldron()
    {
        var cauldronManager = myCauldron.GetComponent<Cauldron>();
        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = false;
        }

        Vector3 newPosition = transform.position;
        newPosition.x += 50.0f;
        newPosition.y = myCauldron.transform.position.y;
        cauldronManager.moveCauldron(newPosition);

        yield return new WaitForSeconds(0.1f);

        while (cauldronManager.currentCollisions != 0)
        {
            newPosition.x += 5.0f;
            cauldronManager.moveCauldron(newPosition);
            yield return new WaitForSeconds(0.001f);
        }

        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = true;
        }
        StopCoroutine(malus_bringOtherCauldron());
    }

    IEnumerator bonus_teleportCauldronCenter()
    {
        var cauldronManager = myCauldron.GetComponent<Cauldron>();
        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = false;
        }
        Vector3 newPosition = new Vector3();
        GameObject centerOfMapRef;

        if (transform.name == "Player1")
        {
            centerOfMapRef = gameManager.transform.GetChild(0).GetChild(0).gameObject;
            if(centerOfMapRef.transform.name == "COM_Cauldron1")
            {
                newPosition = centerOfMapRef.transform.position;
            }
            else
            {
                Debug.LogError("Pas le bon ordre dans le game manager !");
                StopCoroutine(bonus_teleportCauldronCenter());
            }
        }
        else
        {
            centerOfMapRef = gameManager.transform.GetChild(0).GetChild(1).gameObject;
            if (centerOfMapRef.transform.name == "COM_Cauldron2")
            {
                newPosition = centerOfMapRef.transform.position;
            }
            else
            {
                Debug.LogError("Pas le bon ordre dans le game manager !");
                StopCoroutine(bonus_teleportCauldronCenter());
            }
        }

        newPosition.y = myCauldron.transform.position.y;
        cauldronManager.moveCauldron(newPosition);

        yield return new WaitForSeconds(0.1f);

        while (cauldronManager.currentCollisions != 0)
        {
            newPosition.x += 5.0f;
            cauldronManager.moveCauldron(newPosition);
            yield return new WaitForSeconds(0.001f);
        }

        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = true;
        }
        StopCoroutine(malus_bringOtherCauldron());
    }
    IEnumerator malus_bringOtherCauldron()
    {
        var cauldronManager = otherCauldron.GetComponent<Cauldron>();
        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = false;
        }

        Vector3 newPosition = transform.position;
        newPosition.x += 50.0f;
        newPosition.y = otherCauldron.transform.position.y;
        cauldronManager.moveCauldron(newPosition);

        yield return new WaitForSeconds(0.1f);

        while (cauldronManager.currentCollisions != 0)
        {
            newPosition.x += 5.0f;
            cauldronManager.moveCauldron(newPosition);
            yield return new WaitForSeconds(0.001f);
        }

        foreach (var mesh in cauldronManager.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = true;
        }
        StopCoroutine(malus_bringOtherCauldron());
    }

    IEnumerator malus_invert_movement()
    {
        var playerController = otherPlayer.GetComponent<PlayerController>();
        playerController.inverted = true;
        yield return new WaitForSeconds(timeInvert);
        playerController.inverted = false;
        StopCoroutine(malus_invert_movement());
    }

    IEnumerator malus_tache()
    {
        tache_autre_joueur.SetActive(true);
        yield return new WaitForSeconds(timeInk);
        tache_autre_joueur.SetActive(false);
    }

    IEnumerator bonus_speed()
    {
        var playerController = GetComponent<PlayerController>();
        playerController.speed += speedToAdd;
        yield return new WaitForSeconds(timeMoreSpeed);
        playerController.speed -= speedToAdd;
        StopCoroutine(bonus_speed());
    }

    IEnumerator malus_speed()
    {
        var playerController = otherPlayer.GetComponent<PlayerController>();
        playerController.speed -= speedToRemove;
        yield return new WaitForSeconds(timeInk);
        playerController.speed += speedToRemove;
        StopCoroutine(malus_speed());
    }

    void malus_invert_cauldron()
    {
        Vector3 tempPosition = myCauldron.transform.position;
        myCauldron.transform.position = otherCauldron.transform.position;
        otherCauldron.transform.position = tempPosition;
    }


    public IEnumerator giveNewSpells()
    {
        while(true) //todo add listener to game finished
        {
            if (bonus == Bonus.NONE)
            {
                bonus = (Bonus)Random.Range(1, System.Enum.GetValues(typeof(Bonus)).Length);
            }
            if (malus == Malus.NONE)
            {
                malus = (Malus)Random.Range(1, System.Enum.GetValues(typeof(Malus)).Length);
            }
            Debug.Log(transform.name + " :\nBonus is :  " + bonus + " and Malus is : " + malus);
            yield return new WaitForSeconds(timeToNextSpells);
        }
    }

    public string getMalus()
    {
        if (malus == Malus.BRING_OTHER_CAULDRON)
        {
            return "Ramene l'autre chaudron à toi";
        }
        else if (malus == Malus.INK)
        {
            return "Gene la vision de ton adversaire";
        }
        else if (malus == Malus.INVERT_CAULDRON)
        {
            return "Inverse les chaudrons de place";
        }
        else if (malus == Malus.INVERT)
        {
            return "Rend confus ton adversaire";
        }
        else if (malus == Malus.SPEED)
        {
            return "Ralentis ton adversaire";
        }
        else if (malus == Malus.NONE)
        {
            return "Pas de MANA";
        }

        return "Pas de MANA";
    }


    public string getBonus()
    {
        if (bonus == Bonus.SPEED)
        {
            return "Boost de vitesse";
        }
        else if (bonus == Bonus.TELEPORT_CAULDRON_CENTER)
        {
            return "Ramene ton chaudron au centre";
        }
        else if (bonus == Bonus.BRING_CAULDRON)
        {
            return "Ramene ton chaudron a toi";
        }
        else if (bonus == Bonus.NONE)
        {
            return "Pas de MANA";
        }

        return "Pas de MANA";
    }
}
