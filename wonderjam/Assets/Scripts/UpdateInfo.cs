﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpdateInfo : MonoBehaviour {
    [SerializeField]
    private Cauldron cauldron;
    public GameObject gameManager;
    private GameObject objImage;
    private Image image;
    Sprite sprite;
    public TMP_Text info;
    [SerializeField]
    private int PlayerIndex;

    public void Start() {
        if (PlayerIndex == 0)
            objImage = GameObject.Find("Sprite1");

        else
            objImage = GameObject.Find("Sprite2");

        image = objImage.GetComponent<Image>();
    }

    private void Update() {
        DisplayInfo();
    }

    private int UpdateProgression() {
        return 100 - (100 * cauldron.GetRecipe().Count / cauldron.GetnbIngredients());
    }

    private string UpdateCurrentIngredient() {
        return cauldron.GetRecipe()[0].title;
    }

    private string UpdateCurrentDescription() {
        return cauldron.GetRecipe()[0].description;
    }

    private void DisplayInfo() {

        if (gameManager.GetComponent<Gamemode>().isMutual) {
            info.text = "Prochain ingredient : " + gameManager.GetComponent<Gamemode>().mutualCauldron.GetComponent<Cauldron>().GetRecipe()[0].title;

            string temp = gameManager.GetComponent<Gamemode>().mutualCauldron.GetComponent<Cauldron>().GetRecipe()[0].title;
            sprite = Resources.Load<Sprite>("Sprites/Items/" + temp);
            image.sprite = sprite;
            string ingredient = gameManager.GetComponent<Gamemode>().mutualCauldron.GetComponent<Cauldron>().GetRecipe()[0].title;
            if (ingredient != null) {
                info.text = "Prochain ingredient : " + ingredient;
            }

        } else {
            info.text = "Progression - " + UpdateProgression() + "%\n" + UpdateCurrentIngredient();

            string temp = UpdateCurrentIngredient(); ;
            sprite = Resources.Load<Sprite>("Sprites/Items/" + temp);
            image.sprite = sprite;
            info.text = "Progression - " + UpdateProgression() + "%\n" + UpdateCurrentIngredient();
        }

    }
}
