﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Intro_Mngr : MonoBehaviour
{
    public string next_scene;
    public KeyCode keycode = KeyCode.Return;
    public Canvas controllerMap;
    private bool controllersShown = false;

    void Update()
    {
        //  if any key pressed
        if (Input.anyKeyDown)
        {
            if ((controllerMap.isActiveAndEnabled)&&(controllersShown))
            {
                //  load "next_scene"
                //  it must be defined in "Build Settings" before 
                SceneManager.LoadScene("Scenes/" + next_scene);
            }
            else
            {
                int nbController = Input.GetJoystickNames().Length;
                if (nbController == 0)
                {
                    controllerMap.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                }
                else if (nbController == 1)
                {
                    controllerMap.gameObject.transform.GetChild(1).gameObject.SetActive(true);
                }
                else
                {
                    controllerMap.gameObject.transform.GetChild(2).gameObject.SetActive(true);
                }
                controllerMap.gameObject.SetActive(true);
                Time.timeScale = 1;
                controllersShown = true;
            }
        }
    }
}
