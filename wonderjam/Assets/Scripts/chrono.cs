﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class chrono : MonoBehaviour
{
    public float timeRemaining = 10;
    public bool timerIsRunning = false;
    public TMP_Text timeText;
    public TMP_Text fire1StateText;
    public TMP_Text fire2StateText;
    public TMP_Text player1SpellsText;
    public TMP_Text player2SpellsText;

    public GameObject objCauldron1 = null;
    public GameObject objCauldron2 = null;

    private Cauldron cauldron1 = null;
    private Cauldron cauldron2 = null;

    public GameObject player1 = null;
    public GameObject player2 = null;

    public GameObject objFire1 = null;
    public GameObject objFire2 = null;

    public Fire_Mngr fire1 = null;
    public Fire_Mngr fire2 = null;

    public spells_manager spellP1;
    public spells_manager spellP2;

    private GameObject gameManager;

    private void Start()
    {        
        objCauldron1 = GameObject.Find("Cauldron1");
        objCauldron2 = GameObject.Find("Cauldron2");

        player1 = GameObject.Find("Player1");
        player2 = GameObject.Find("Player2");

        objFire1 = GameObject.Find("Fire1");
        objFire2 = GameObject.Find("Fire2");

        cauldron1 = objCauldron1.GetComponent<Cauldron>();
        cauldron2 = objCauldron2.GetComponent<Cauldron>();

        fire1 = objFire1.GetComponent<Fire_Mngr>();
        fire2 = objFire2.GetComponent<Fire_Mngr>();

        gameManager = GameObject.Find("GameManager");
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;

                Debug.Log("1");
                gameManager.GetComponent<Gamemode>().gameIsFinished.Invoke(WinnerEndTimer(cauldron1, cauldron2, player1, player2), (int)GameOverType.TIME_OVER);
            }

            DisplayFireState(fire1, fire2);
            DisplaySpells(spellP1, spellP2);
        }

        
        
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void DisplayFireState(Fire_Mngr fire1, Fire_Mngr fire2)
    {
        fire1StateText.text = "Feu : " + fire1.GetFireState();
        fire2StateText.text = "Feu : " + fire2.GetFireState();
    }

    void DisplaySpells(spells_manager spell1, spells_manager spell2)
    {
        player1SpellsText.text = "BONUS\n" + spell1.getBonus() + "\nMALUS\n" + spell1.getMalus();
        player2SpellsText.text = "BONUS\n" + spell2.getBonus() + "\nMALUS\n" + spell2.getMalus();
    }

    GameObject WinnerEndTimer(Cauldron cauldron1, Cauldron cauldron2, GameObject player1, GameObject player2)
    {
        if (cauldron1.GetRecipe().Count > cauldron2.GetRecipe().Count)
        {
            return player1;
        }

        else
        {
            return player2;
        }
    }
}
