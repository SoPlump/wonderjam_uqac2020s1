﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

enum GameOverType
{
    FIRE_EXTINGUISHED,
    RECIPE_FINISHED,
    TIME_OVER
}

[System.Serializable]
public class GameIsFinished : UnityEvent<GameObject, int>
{
}

public class Gamemode : MonoBehaviour
{
    GameObject winner;
    public GameObject gameOverScreen;

    [System.NonSerialized]
    public GameIsFinished gameIsFinished;
    public List<GameObject> fires;
    public List<GameObject> cauldrons;
    public List<GameObject> players = new List<GameObject>();

    [Header("Scenario")]
    public GameObject mutualCauldron;
    [Header("Scenario")]
    public GameObject chrono;
    [System.NonSerialized]
    public bool isMutual;
    public ParticleSystem smoke;

    public ItemDatabase itemDatabase;

    public ParticleSystem explosion_particles;
    public GameObject explosion_object;

    private AudioSource screen_audioSource;

    [SerializeField]
    private AudioClip audioResultScreen = null;

    [SerializeField]
    private AudioClip audioIntro = null;
    [SerializeField]
    private AudioClip firstIntro = null;

    [SerializeField]
    private AudioClip audioAddItem = null;

    //screen_audioSource.PlayOneShot(audioAddItem);



    // Recipe
    public int numberOfItemInRecipes = 5;

    // Start is called before the first frame update
    void Start()
    {
        
        screen_audioSource = GetComponent<AudioSource>();
        screen_audioSource.PlayOneShot(firstIntro);

        if (!mutualCauldron)
            mutualCauldron = GameObject.Find("MutualCauldron");
        if (!chrono)
            chrono = GameObject.Find("Chrono");

                    isMutual = true;
        if (gameIsFinished == null)
            gameIsFinished = new GameIsFinished();

        gameIsFinished.AddListener(GameOver);

        screen_audioSource = GetComponent<AudioSource>();


        foreach(var cauldron in cauldrons)
        {
            cauldron.SetActive(false);
            cauldron.GetComponent<Cauldron>().GenerateRecipes(numberOfItemInRecipes, itemDatabase);
            foreach (var mesh in cauldron.GetComponentsInChildren<Renderer>())
            {
                mesh.enabled = false;
            }
        }

        mutualCauldron.GetComponent<Cauldron>().GenerateRecipes(1, itemDatabase);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ItemAdded()
    {

        StartCoroutine(explosion());

        if (isMutual)
        {
            screen_audioSource.Stop();
            screen_audioSource.PlayOneShot(audioIntro);


            foreach (var cauldron in cauldrons)
            {
                cauldron.SetActive(true);
                foreach (var mesh in cauldron.GetComponentsInChildren<Renderer>())
                {
                    mesh.enabled = true;
                }
            }


            foreach (var fire in fires)
            {
                fire.GetComponent<Fire_Mngr>().StartCoroutine(fire.GetComponent<Fire_Mngr>().Extinguish());
            }

            foreach (var player in players)
            {
                var spell = player.GetComponent<spells_manager>();
                spell.StartCoroutine(spell.giveNewSpells());
            }
            chrono.GetComponent<chrono>().timerIsRunning = true;
            smoke.gameObject.SetActive(true);
        }
    }

    IEnumerator explosion()
    {
        explosion_object.SetActive(true);
        explosion_particles.Play(true);
        yield return new WaitForSeconds(0.5f);
        mutualCauldron.SetActive(false);
        foreach (var mesh in mutualCauldron.GetComponentsInChildren<Renderer>())
        {
            mesh.enabled = false;
        }
    }

        void GameOver(GameObject player, int victoryType)
    {
        if (!isMutual)
        {
            if (!winner)
            {
                screen_audioSource.PlayOneShot(audioResultScreen);
                Time.timeScale = 0;
                winner = player;
                foreach (var fire in fires)
                {
                    fire.GetComponent<Fire_Mngr>().StopAllCoroutines();
                }

                string text = "";
                GameOverType gameOverType = (GameOverType)victoryType;
                switch (gameOverType)
                {
                    case GameOverType.FIRE_EXTINGUISHED:
                        text = player.name + " a gagné ! Il semblerait que tu sais mieux gérer ton feu...";
                        Debug.Log(player.name + " a gagné ! Il semblerait que tu sais mieux gérer ton feu...");
                        break;
                    case GameOverType.RECIPE_FINISHED:
                        text = player.name + " a gagné ! Il a maîtrisé à la perfection sa magie pour créer la meilleure potion...";
                        Debug.Log(player.name + " a gagné ! Il a maîtrisé à la perfection sa magie pour créer la meilleure potion...");
                        break;
                    case GameOverType.TIME_OVER:
                        text = "Temps écoulé !\nLes 2 magiciens se sont perdus, mais\n" + player.name + "était le plus avancé dans sa potion.\nIl gagne !";
                        Debug.Log(player.name + " a gagné ! Il a maîtrisé à la perfection sa magie pour créer la meilleure potion...");
                        break;
                }

                gameOverScreen.gameObject.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = text;
                gameOverScreen.SetActive(true);
            }
        }
        else
        {
            isMutual = false;
        }
    }
}
