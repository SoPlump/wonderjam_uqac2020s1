﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Cauldron : MonoBehaviour
{
    [System.NonSerialized]
    public bool isMoving = false;

    [SerializeField]
    public Fire_Mngr fire;

    public GameObject player;

    private GameObject gameManager;

    [System.NonSerialized] 
    public int currentCollisions = 0;
    [System.NonSerialized]
    public UnityEvent addIngredient;

    private List<Item> recipe = new List<Item>();
    private int nbIngredients = 0;

    // Start is called before the first frame update
    void Start()
    {

        if (addIngredient == null)
            addIngredient = new UnityEvent();

        gameManager = GameObject.Find("GameManager");
    }

    public void GenerateRecipes(int numberOfItemInRecipes, ItemDatabase itemDatabase)
    {
        int currentNb = 0;
        nbIngredients = numberOfItemInRecipes;
        while (currentNb < numberOfItemInRecipes / 4)
        {
            recipe.Add(itemDatabase.items_forest[Random.Range(0, itemDatabase.items_forest.Count)]);
            ++currentNb;
        }
        while (currentNb < numberOfItemInRecipes / 4 * 2)
        {
            recipe.Add(itemDatabase.items_lac[Random.Range(0, itemDatabase.items_lac.Count)]);
            ++currentNb;
        }
        while (currentNb < numberOfItemInRecipes / 4 * 3)
        {
            recipe.Add(itemDatabase.items_flower[Random.Range(0, itemDatabase.items_flower.Count)]);
            ++currentNb;
        }
        while (currentNb < numberOfItemInRecipes)
        {
            recipe.Add(itemDatabase.items_rocks[Random.Range(0, itemDatabase.items_rocks.Count)]);
            ++currentNb;
        }
        if (numberOfItemInRecipes == 1)
        {
            recipe[0] = (itemDatabase.items[Random.Range(0, itemDatabase.items.Count)]);
            if (!recipe[0].food)
            {
                recipe[0] = itemDatabase.items_forest[Random.Range(0, itemDatabase.items_forest.Count)];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CheckRecipesSize()
    {
        if (recipe.Count > 0)
        {
            return true;
        }

        return false;
    }
    public List<Item> GetRecipe() {
        return recipe;
    }

    public int GetnbIngredients() {
        return nbIngredients;
    }

    public bool CheckIngredient(Item item)
    {
        if (item.title == recipe[0].title)
        {
            Debug.LogError("Bon ingredient " + item.title);

            return true;
        }

        Debug.LogError("Mauvais ingredient " + item.title);

        return false;
    }

    public void SendToCauldron()
    {
        recipe.RemoveAt(0);

        gameManager.GetComponent<Gamemode>().ItemAdded();

        if (recipe.Count == 0)
        {
            gameManager.GetComponent<Gamemode>().gameIsFinished.Invoke(player, (int)GameOverType.RECIPE_FINISHED);
        }
    }

    public void moveCauldron(Vector3 newPos)
    {
        isMoving = true;
        //gameObject.SetActive(false);
        transform.position = newPos;
    }

    private void OnTriggerEnter(Collider other)
    {
        ++currentCollisions;
    }

    private void OnTriggerExit(Collider other)
    {
        --currentCollisions;
    }
}
