﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private int playerIndex;

    public float speed = 10f;
    [System.NonSerialized]
    public float dashDistance = 1.0f;
    [System.NonSerialized]
    public float dashSpeed = 0.0f;
    public bool inverted = false;

    public GameObject run_effect;
    private bool pos_static;

    private List<KeyCode> action;
    private List<KeyCode> dash;
    private KeyCode bonus;
    private string bonus_malus_controller;
    private KeyCode malus;
    private List<KeyCode> drop;
    private List<KeyCode> inventoryIndex;

    public GameObject wizard_model;

    [SerializeField] private AudioClip audioGrab = null;
    private AudioSource perso_audioSource;

    public bool IsMoving {
        get {
            if (playerIndex == 0) {
                if (ConnectedGamepadNumber() == 2)
                    return (Input.GetAxisRaw("HorizontalGp2") != 0 || Input.GetAxisRaw("VerticalGp2") != 0);
                return (Input.GetAxisRaw("HorizontalKey1") != 0 || Input.GetAxisRaw("HorizontalKey1") != 0);
            }
            if (ConnectedGamepadNumber() == 0)
                return (Input.GetAxisRaw("HorizontalKey2") != 0 || Input.GetAxisRaw("VerticalKey2") != 0);
            return (Input.GetAxisRaw("HorizontalGp1") != 0 || Input.GetAxisRaw("VerticalGp1") != 0);
        }
    }

    private void Start() {
        if (playerIndex == 0) {
            action = new List<KeyCode>() { KeyCode.Space, KeyCode.Joystick2Button2 };
            dash = new List<KeyCode>() { KeyCode.LeftShift, KeyCode.Joystick2Button0 };
            drop = new List<KeyCode>() { KeyCode.LeftControl, KeyCode.Joystick2Button3 };
            inventoryIndex = new List<KeyCode>() { KeyCode.Tab, KeyCode.Joystick2Button5 };
            bonus = KeyCode.E;
            bonus_malus_controller = "TriggerGp2";
            malus = KeyCode.Q;
        } else {
            action = new List<KeyCode>() { KeyCode.K, KeyCode.Joystick1Button2 };
            dash = new List<KeyCode>() { KeyCode.RightShift, KeyCode.Joystick1Button0 };
            drop = new List<KeyCode>() { KeyCode.RightControl, KeyCode.Joystick1Button3 };
            inventoryIndex = new List<KeyCode>() { KeyCode.AltGr, KeyCode.Joystick1Button5 };
            bonus = KeyCode.L;
            bonus_malus_controller = "TriggerGp1";
            malus = KeyCode.J;
        }

        run_effect.SetActive(false);
        pos_static = true;
    }

    public bool ActionKey() {
        return (Input.GetKeyDown(action[0]) || Input.GetKeyDown(action[1]));
    }

    public bool DashKey() {
        return (Input.GetKeyDown(dash[0]) || Input.GetKeyDown(dash[1]));
    }

    public bool DropKey() {
        return (Input.GetKeyDown(drop[0]) || Input.GetKeyDown(drop[1]));
    }

    public bool BonusKey() {
        return (Input.GetKeyDown(bonus) || Input.GetAxis(bonus_malus_controller) > 0);
    }

    public bool MalusKey() {
        return (Input.GetKeyDown(malus) || Input.GetAxis(bonus_malus_controller) < 0);
    }

    public bool SelectItem() {
        return (Input.GetKeyDown(inventoryIndex[0]) || Input.GetKeyDown(inventoryIndex[1]));
    }


    private int ConnectedGamepadNumber() {
        return Input.GetJoystickNames().Length;
    }

    void FixedUpdate() {
        int nbGp = ConnectedGamepadNumber();
        Move(nbGp);
    }

    void Move(int gamepad) {
        if (playerIndex == 0) {// Player 1
            if (gamepad == 2)
                makeMove("HorizontalGp2", "VerticalGp2");
            else
                makeMove("HorizontalKey1", "VerticalKey1");
        } else { // Player 2
            if (gamepad == 0)
                makeMove("HorizontalKey2", "VerticalKey2");
            else
                makeMove("HorizontalGp1", "VerticalGp1");
        }
        makeDash();
    }

    void makeMove(string hAxis, string vAxis) {
        Vector3 move = Vector3.zero;
        move.x = Input.GetAxis(hAxis);
        move.z = Input.GetAxis(vAxis);

        if (inverted) {
            move.x = -move.x;
            move.z = -move.z;
        }

        if (move.magnitude > 1)
            move = move.normalized;

        if (dashSpeed == 0)
            GetComponent<Rigidbody>().MovePosition(transform.position + move * speed * Time.fixedDeltaTime);


        if (move.x != 0 || move.z != 0) // orientation
        {
            wizard_model.transform.forward = move;
            run_effect.SetActive(true);
            wizard_model.transform.Rotate(15.0f, 0.0f, 0.0f, Space.Self);
            pos_static = true;
        } else if (pos_static) {
            run_effect.SetActive(false);
            wizard_model.transform.Rotate(-15.0f, 0.0f, 0.0f, Space.Self);
            pos_static = false;
        }

    }

    void makeDash() {
        if (dashSpeed != 0) {
            GetComponent<Rigidbody>().MovePosition(transform.position + wizard_model.transform.forward * dashDistance * dashSpeed * Time.fixedDeltaTime);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if ((other.transform.name.Contains("Player")) && (dashSpeed != 0)) {
            Debug.Log(other.transform.name + " fait tomber objet");
            other.gameObject.GetComponent<Player>().DropObjectOnGround();
        }
    }

    /*
    void keyboardMove() {
        if (Input.GetKeyDown(up))
            transform.Translate(new Vector3(3, 0, 0) * speed * Time.deltaTime);
        else if (Input.GetKeyDown(down))
            transform.Translate(new Vector3(-3, 0, 0) * speed * Time.deltaTime);
        else if (Input.GetKeyDown(left))
            transform.Translate(new Vector3(0, 0, -3) * speed * Time.deltaTime);
        else if (Input.GetKeyDown(right))
            transform.Translate(new Vector3(0, 0, 3) * speed * Time.deltaTime);
    }*/
}
