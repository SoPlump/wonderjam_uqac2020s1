﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 enum FireState
{
    EXTINGUISHED,
    WEAK,
    MEDIUM,
    STRONG,
}

public class Fire_Mngr : MonoBehaviour
{
    private GameObject gameManager;
    public GameObject player;
    public GameObject otherPlayer;
    public KeyCode touche_clavier_addFire = KeyCode.Space;

    public float fireIntensity = 20.0f;
    private FireState fireState;

    public float extinguishingTimePerSecond = 1.0f;

    public float maxIntensity;
    public float strongThreshold = 0.15f;
    public float mediumThreshold = 0.10f;
    public float weakThreshold = 0.05f;

    public float addingWoodTime = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");

        if (fireIntensity < weakThreshold)
        {
            fireState = FireState.EXTINGUISHED;
        }
        else if (fireIntensity < mediumThreshold)
        {
            fireState = FireState.WEAK;
        }
        else if (fireIntensity < strongThreshold)
        {
            fireState = FireState.MEDIUM;
        }
        else if (fireIntensity > strongThreshold)
        {
            fireState = FireState.STRONG;
        }

        Debug.Log(fireState);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateFireVisual();

        //if (Input.GetKeyDown(touche_clavier_addFire))
        //{
        //    fireIntensity += addingWoodTime;
        //    Mathf.Clamp(fireIntensity, 0.0f, maxIntensity);
        //}
    }

    private void UpdateFireVisual()
    {
        gameObject.transform.Find("fire_strong").gameObject.SetActive(false);
        gameObject.transform.Find("fire_medium").gameObject.SetActive(false);
        gameObject.transform.Find("fire_low").gameObject.SetActive(false);

        if (GetFireState().Equals("Fort"))
        {
            gameObject.transform.Find("fire_strong").gameObject.SetActive(true);
        }
        else if (GetFireState().Equals("Moyen"))
        {
            gameObject.transform.Find("fire_medium").gameObject.SetActive(true);
        }
        else if (GetFireState().Equals("Faible"))
        {
            gameObject.transform.Find("fire_low").gameObject.SetActive(true);
        }
    }

    public string GetFireState()
    {
        if (fireState == FireState.EXTINGUISHED)
        {
            return "Eteint";
        }
        else if (fireState == FireState.WEAK)
        {
            return "Faible";
        }
        else if (fireState == FireState.MEDIUM)
        {
            return "Moyen";
        }
        else if (fireState == FireState.STRONG)
        {
            return "Fort";
        }

        return "";
    }

    public void addWood()
    {
        fireIntensity += addingWoodTime;
        Mathf.Clamp(fireIntensity, 0.0f, maxIntensity);

        StopCoroutine(ExtinguishedChrono());
    }

    public IEnumerator Extinguish()
    {
        while(fireIntensity > 0.0f)
        {
            fireIntensity -= extinguishingTimePerSecond;
            //Debug.Log(fireIntensity);

            if (fireIntensity < weakThreshold)
            {
                if (fireState != FireState.EXTINGUISHED)
                {
                    fireState = FireState.EXTINGUISHED;
                    Debug.Log(player.name + " fire : EXTINGUISHED");
                    StartCoroutine("ExtinguishedChrono");
                }
            }
            else if (fireIntensity < mediumThreshold)
            {
                if (fireState != FireState.WEAK)
                {
                    fireState = FireState.WEAK;
                    Debug.Log(player.name + " fire : WEAK");
                    StopCoroutine("ExtinguishedChrono");
                }
            }
            else if (fireIntensity < strongThreshold)
            {
                if (fireState != FireState.MEDIUM)
                {
                    fireState = FireState.MEDIUM;
                    Debug.Log(player.name + " fire : MEDIUM");
                    StopCoroutine("ExtinguishedChrono");
                }
            }
            else if (fireIntensity > strongThreshold)
            {
                if (fireState != FireState.STRONG)
                {
                    fireState = FireState.STRONG;
                    Debug.Log(player.name + " fire : STRONG");
                    StopCoroutine("ExtinguishedChrono");
                }
            }

            yield return new WaitForSeconds(1.0f);
        }
    }

    IEnumerator ExtinguishedChrono()
    {
        yield return new WaitForSeconds(weakThreshold);

        if (fireState == FireState.EXTINGUISHED)
        {
            gameManager.GetComponent<Gamemode>().gameIsFinished.Invoke(otherPlayer, (int)GameOverType.FIRE_EXTINGUISHED);
            StopCoroutine("Extinguish");
        }
    }
}
